/*  Cracking the code interview 5th Edition 
 *  Problem #1.1 
 *  Implementation by Guillaume-Olivier Gagnon */

import java.io.*;
import java.util.*;

public class Unun 

{
	
		public static HashMap<Character,Integer> createTable(String str)
		{
			HashMap<Character,Integer> tableMap = new HashMap<Character,Integer>();
			char[] charArray = str.toCharArray();
			
			
			for (char c : charArray )
			{
				if (tableMap.containsKey(c)){
					tableMap.put(c, tableMap.get(c)+1);
					
				}
				else
				{
					tableMap.put(c,1);
				}
				
			}
			
			return tableMap;
		}

		public static boolean checkIfCharUnique(HashMap<Character,Integer> tableMap)
		{
			for (Integer value: tableMap.values())
			{
				if (value > 1)
				{return false;}
			}
				
			return true;
	
			
		}
		

	public static void main(String[] args) throws java.lang.Exception
	{
	BufferedReader r = new BufferedReader (new InputStreamReader (System.in));
	
	String s;
	s = r.readLine();

	System.out.println(checkIfCharUnique(createTable(s)));
		
		
	}
	
}

