/***************************** Include Files *********************************/

#include "xparameters.h"
#include "xgpio.h"



/************************** Constant Definitions *****************************/

/*
 * The following constant maps to the name of the hardware instances that
 * were created in the EDK XPS system.
 */
#define GPIO_EXAMPLE_DEVICE_ID  XPAR_AXI_GPIO_0_DEVICE_ID
#define LED_CHANNEL 1


/************************** Variable Definitions *****************************/

/*
 * The following are declared globally so they are zeroed and so they are
 * easily accessible from a debugger
 */

XGpio Gpio; /* The Instance of the GPIO Driver */



/************************** Main *****************************/
int main(void)
{

	int Status;
	u32 value = 0;
	u32 period = 0;
	u32 brightness = 0;
	/*
	* Initialize the GPIO driver
	*/
	Status = XGpio_Initialize(&Gpio, GPIO_EXAMPLE_DEVICE_ID);
	if (Status != XST_SUCCESS)
		{return XST_FAILURE;}

	// Clear LEDs
	XGpio_DiscreteWrite(&Gpio, LED_CHANNEL, 0);

	while (1)
	{
		print("Select a Brightness between 0 and 9\n\r");
		value = inbyte();

		period = value - 0x30;
		xil_printf("Brightness Level %d selected\n\r", period);

		brightness = period * 110000; // LE6 CLK cycles, so up to 1M (0-9 -> 0-999000)

		XGpio_DiscreteWrite(&Gpio, LED_CHANNEL, brightness);

	}

	return XST_SUCCESS;

}
